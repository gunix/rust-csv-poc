#!/usr/bin/env bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
TEST_DIR="${SCRIPT_DIR}/test_cases"
cd "$SCRIPT_DIR"

for i in {1..4}; do
  echo "---> TEST #${i}"

  # sort is required because rust will output in random order
  cargo run -q -- "${TEST_DIR}/transactions_${i}.csv" \
    2> "${TEST_DIR}/transactions_${i}.csv.errors.tmp" |
    sort -r > "${TEST_DIR}/accounts_${i}.csv.tmp"

  # test the correctness of stderr (alert simulation for weird scenarios)
  diff "${TEST_DIR}/transactions_${i}.csv.errors.tmp" "${TEST_DIR}/transactions_${i}.csv.errors" &&
    echo "---> stderr OK" ||
    echo "---> stderr FAILED"

  # test the correctness of stdout (account output requested by challenge)
  diff "${TEST_DIR}/accounts_${i}.csv" "${TEST_DIR}/accounts_${i}.csv.tmp" &&
    echo "---> stdout OK" ||
    echo "---> stdout FAILED"
  echo
done
