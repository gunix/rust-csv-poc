# Rust - CSV - POC

This is a small proof of concept of how to operate CSVs in Rust. The implementation might seem odly specific, since it was part of a challenge. Given a list of transactions, this service returns the account balance of each accound holder. The app also handles customer claims of erroneous transactions, which can be in any of the following states: "Disputed, Resolved, Chargeback".

## Design

Assumptions based on my understanding of the challenge description:
  * the CSV has a valid format
    * all records contain 3 commas, including disputes, resolves and chargebacks
    * unknown transaction types should be handled gracefully
  * the transactions are not an immutable history, but incoming requests
    * this means that the engine can reject transactions
    * the engine must also lock accounts in case of malicious behavior
  * both client IDs and transaction IDs are unique 
  * transactions are in chronological order, however IDs are not ordered
  * transactions can be reverted
    * code will not be space-efficient (history of all transactions is required)
  * transactions can be duplicates
    * not explicit in the challenge, so assuming worst case
  * Resolves and Chargebacks should be ignored in the absence of a Dispute
    * as transactions are in chronological order, we can drop requests if a Dispute is not present
    * a history of all disputes is required, to handled the absence of disputes and duplicates
  * though output of accounts should go to stdout, some form of error would be nice
    * this would make sense for a bank or an ATM
    * printing errors to stderr showcases the behavior of error handling
    * normally the errors should be forwarded to an alert manager (like opsgenie)

## Testing

As rust `#[cfg(test)]` is not ideal for testing stdin and stdout, the repo features a bash script called `test.sh` that will test input and output against a batch of tests. Options are available to test from Rust, but I prefer to KISS.

Testing can be performed by running `./test.sh`. Both stdout and stderr will be outputed to specific files, so that the correctness can be tested. The test cases can be found in `test_cases/`.
```sh
---> TEST #1
---> stderr OK
---> stdout OK

---> TEST #2
---> stderr OK
---> stdout OK

---> TEST #3
---> stderr OK
---> stdout OK

---> TEST #4
---> stderr OK
---> stdout OK
```

## Alternative solutions

Alternative implementations are possible.

The most obvious alternative is to record a ledger for each account, meaning that the hashmap of transcations is recorded individually for eachaccount. As all transactions are comming in together with the the client ID, having transactions under each account would offer very good complexity, as you can query the `accounts` hashmap to get the account and after that within the account you can query the `transactions` hashmap to get the specific transaction. In case a history of transactions pe account is desired, this is the best implementation.

If we consider the history of transactions to be immutable and this app to be only a report tool, the architecture changes a bit. If the transactions already happened and they can not be changed, the tool must process every single transaction, even if the balance of the account will be below 0. In this case, locking accounts would trigger an alert, but further transactions wouldn't get refused.

If this microservice should be a source of truth of transactions and account balance, meaning precise data and a history of all events are desired, the cleanest implementation would be to use a database that offers synchronous replication and high availability (for example, a galera cluster), or to write a similar system in rust (to make sure multiple services confirm a transcation before it gets commited).
